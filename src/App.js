import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Gauge from 'react-canvas-gauge';

import {
  Alert, Card, CardBody, CardHeader, Col, Row
} from 'reactstrap';
import { Bar } from 'react-chartjs-2';
import io from 'socket.io-client';

import './App.scss';

const DEGREE_UNIT = decodeURI('%C2%B0C');

class App extends Component {
  static propTypes = {
    batState: PropTypes.object,
    refreshBatState: PropTypes.func
  };

  static defaultProps = {
    batState: {
      deviceId: '',
      deviceType: '',
      DESIGN_VOLT: 18000,
      SOC: 0,
      CURRENT: 0,
      TEMPERATURE: 30.0,
      CELL_VOLT: [],
      TOTAL_VOLT: 0,
      DELTA_VOLT: 0,
      ALARM_FLAG: {
        OV: false,
        UV: false
      }
    },
    refreshBatState: (data) => { console.log(data); }
  };

  constructor(props) {
    super(props);

    const dv = parseInt((18000) * 0.0015, 10);
    const tmpVoltInterval1Value = parseInt(dv * 0.1, 10);
    const dc = parseInt((10000) * 0.001, 10);
    let tmpDC = dc;
    if (dc < 100) {
      tmpDC = 2 * dc;
    } else if (dc < 70) {
      tmpDC = 3 * dc;
    }
    tmpDC += (5 - (tmpDC % 5));
    const tmpInterval1Value = parseInt(tmpDC * 0.2, 10);
    const tmpInterval2Value = parseInt(dc * 0.2, 10);

    this.state = {
      tempValueInfo: {
        minValue: -40,
        scaleList: [
          { quantity: 4, scale: 10, startColor: '#327aff', endColor: '#327aff' },
          { quantity: 4, scale: 5, startColor: '#327aff', endColor: 'orange' },
          { quantity: 4, scale: 20, startColor: 'orange', endColor: 'red' }
        ]
      },
      currentValueInfo: {
        minValue: -1 * dc,
        scaleList: [
          { quantity: 5, scale: tmpInterval1Value, startColor: 'blue', endColor: 'blue' },
          { quantity: 5, scale: tmpInterval2Value, startColor: 'blue', endColor: 'orange' }
        ]
      },
      voltageValueInfo: {
        minValue: 0,
        scaleList: [
          { quantity: 10, scale: tmpVoltInterval1Value, startColor: 'blue', endColor: 'blue' }
        ]
      },
      chartData: {
        labels: [],
        datasets: [
          {
            backgroundColor: [],
            borderColor: [],
            borderWidth: 2,
            hoverBorderWidth: 0,
            data: []
          }
        ]
      },
      chartOpts: {
        maintainAspectRatio: false,
        legend: { display: false },
        scales: {
          xAxes: [{ ticks: { fontStyle: 'bold' }, scaleLabel: { fontSize: 14, fontStyle: 'bold', display: true, labelString: 'Cell Volt Num(mV)' } }],
          yAxes: [{ scaleLabel: { display: true, labelString: 'mV' }, ticks: { min: 0, max: 5000, stepSize: 1000 } }]
        }
      },
      SOC: 0,
      TOTAL_VOLT: 0,
      CURRENT: 0,
      TEMPERATURE: 0,
      enableAlert: false,
      alertMsg: ''
    };
  }

  componentDidMount() {
    const { refreshBatState } = this.props;
    const socket = io('', { path: '/api/socket' });
    socket.on('report', (bat) => {
      refreshBatState(Object.assign({}, bat.tag || {}, bat.data || {}));
      // this.setState({
      //   SOC: batState.SOC || 0,
      //   TOTAL_VOLT: ((batState.TOTAL_VOLT || 0) / 1000),
      //   CURRENT: ((batState.CURRENT || 0) / 1000),
      //   TEMPERATURE: batState.TEMPERATURE || 0.0,
      //   chartData: {
      //     labels,
      //     datasets: [
      //       {
      //         backgroundColor,
      //         borderColor,
      //         borderWidth: 2,
      //         hoverBorderWidth: 0,
      //         data
      //       }
      //     ]
      //   },
      //   chartOpts: {
      //     maintainAspectRatio: false,
      //     legend: { display: false },
      //     scales: {
      //       xAxes: [{ ticks: { fontStyle: 'bold' }, scaleLabel: { fontSize: 14, fontStyle: 'bold', display: true, labelString: `Cell Volt Num(mV)${(batState.DELTA_VOLT !== undefined) ? ` - Delta (${batState.DELTA_VOLT})` : ''}` } }],
      //       yAxes: [{ scaleLabel: { display: true, labelString: 'mV' }, ticks: { min: 0, max: 5000, stepSize: 1000 } }]
      //     }
      //   }
      // });
    });
    socket.on('alert', (data) => {
      this.setState({
        enableAlert: true,
        alertMsg: data.msg
      });
      setTimeout(() => {
        this.setState({
          enableAlert: false
        });
      }, 1500);
    });
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.batState !== prevProps.batState) {
      const batState = this.props.batState || {};

      const cellVolt = batState.CELL_VOLT || [];
      const labels = [];
      const data = [];
      const backgroundColor = [];
      const borderColor = [];
      for (let i = 0; i < cellVolt.length; i += 1) {
        labels.push(`${i + 1} (${cellVolt[i]})`);
        data.push(cellVolt[i]);
        backgroundColor.push('rgba(255,255,0,0.6)');
        borderColor.push('rgba(255,255,0,1)');
      }

      this.setState({
        SOC: batState.SOC || 0,
        TOTAL_VOLT: ((batState.TOTAL_VOLT || 0) / 1000),
        CURRENT: ((batState.CURRENT || 0) / 1000),
        TEMPERATURE: batState.TEMPERATURE || 0.0,
        chartData: {
          labels,
          datasets: [
            {
              backgroundColor,
              borderColor,
              borderWidth: 2,
              hoverBorderWidth: 0,
              data
            }
          ]
        },
        chartOpts: {
          maintainAspectRatio: false,
          legend: { display: false },
          scales: {
            xAxes: [{ ticks: { fontStyle: 'bold' }, scaleLabel: { fontSize: 14, fontStyle: 'bold', display: true, labelString: `Cell Volt Num(mV)${(batState.DELTA_VOLT !== undefined) ? ` - Delta (${batState.DELTA_VOLT})` : ''}` } }],
            yAxes: [{ scaleLabel: { display: true, labelString: 'mV' }, ticks: { min: 0, max: 5000, stepSize: 1000 } }]
          }
        }
      });
    }
  }

  render() {
    const {
      tempValueInfo,
      currentValueInfo,
      voltageValueInfo,
      chartOpts,
      SOC,
      TOTAL_VOLT,
      CURRENT,
      TEMPERATURE,
      chartData,
      enableAlert,
      alertMsg
    } = this.state;

    return (
      <div className="app">
        <Alert color="danger" isOpen={enableAlert}>{alertMsg}</Alert>
        <div className="container">
          <p />
          <Card>
            <CardHeader>
              <h4 style={{ fontWeight: 600 }}>Battery Dashboard</h4>
            </CardHeader>
            <CardBody>
              <Row>
                <Col>
                  <Gauge
                    theme="dark"
                    mode="progress"
                    title="SOC"
                    unit="%"
                    value={SOC}
                  />
                </Col>
                <Col>
                  <Gauge
                    theme="dark"
                    title="Voltage"
                    unit="V"
                    enableColorful={false}
                    scaleList={voltageValueInfo.scaleList}
                    minValue={voltageValueInfo.minValue}
                    value={TOTAL_VOLT}
                  />
                </Col>
                <Col>
                  <Gauge
                    theme="dark"
                    title="Current"
                    unit="A"
                    enableColorful={false}
                    scaleList={currentValueInfo.scaleList}
                    minValue={currentValueInfo.minValue}
                    value={CURRENT}
                  />
                </Col>
                <Col>
                  <Gauge
                    theme="dark"
                    title="Temp."
                    unit={DEGREE_UNIT}
                    enableColorful
                    scaleList={tempValueInfo.scaleList}
                    minValue={tempValueInfo.minValue}
                    value={TEMPERATURE}
                  />
                </Col>
              </Row>
              <p />
              <Row>
                <Col>
                  <Bar
                    data={chartData}
                    options={chartOpts}
                  />
                </Col>
              </Row>
            </CardBody>
          </Card>
        </div>
      </div>
    );
  }
}

export default App;
