import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import actions from './actions';

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
const mapStateToProps = (state = {}) => {
  return {
    batState: state.batState || {}
  };
};
const connectToProps = connect(mapStateToProps, mapDispatchToProps);

export default connectToProps;
