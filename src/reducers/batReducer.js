import {
  INIT_BAT_STATE, REFRESH_BAT_STATE
} from '../actions/actionstype';

const DEFAULT_STATE = Object.freeze({
  deviceId: '',
  deviceType: '',
  DESIGN_VOLT: 18000,
  SOC: 0,
  CURRENT: 0,
  TEMPERATURE: 30.0,
  CELL_VOLT: [],
  TOTAL_VOLT: 0,
  DELTA_VOLT: 0,
  ALARM_FLAG: {
    OV: false,
    UV: false
  }
});

export default (state = DEFAULT_STATE, action = {}) => {
  switch (action.type) {
    case REFRESH_BAT_STATE: {
      return Object.assign({}, state, action.batState || DEFAULT_STATE);
    }
    case INIT_BAT_STATE: {
      return Object.assign({}, DEFAULT_STATE);
    }
    default: {
      return state;
    }
  }
};
