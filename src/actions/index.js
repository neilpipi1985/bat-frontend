import { refreshBatState, resetBatState } from './batAction';

const actions = {
  refreshBatState,
  resetBatState
};

export default actions;
