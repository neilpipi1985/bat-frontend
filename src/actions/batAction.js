import {
  INIT_BAT_STATE, REFRESH_BAT_STATE
} from './actionstype';

export function refreshBatState(batState) {
  return { type: REFRESH_BAT_STATE, batState };
}

export function resetBatState() {
  return { type: INIT_BAT_STATE };
}
